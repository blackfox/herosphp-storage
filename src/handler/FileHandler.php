<?php

// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// * Copyright 2014 The Herosphp Authors. All rights reserved.
// * Use of this source code is governed by a MIT-style license
// * that can be found in the LICENSE file.
// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

declare(strict_types=1);

namespace herosphp\plugin\storage\handler;

/**
 * file operation handler
 * ---------------------------------------------------
 * @author RockYang<yangjian102621@gmail.com>
 */
interface FileHandler
{
    /**
     * save upload file
     * @return array(string, error)
     */
    public function save(string $srcFile, string $filename): array;

    /**
     * save base64 data into file
     * @return array(string, error)
     */
    public function saveBase64(string $data, string $filename): array;

    /**
     * list files
     * @return array(array, error)
     */
    public function list(int $page, int $pageNo): array;

    /**
     * delete the specified file
     * @return array(bool, error)
     */
    public function delete(string $filename): array;
}
