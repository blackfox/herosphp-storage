<?php

// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// * Copyright 2014 The Herosphp Authors. All rights reserved.
// * Use of this source code is governed by a MIT-style license
// * that can be found in the LICENSE file.
// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

declare(strict_types=1);

namespace herosphp\plugin\storage\core;

/**
 * 文件管理工具，获取文件列表，或者删除文件
 * ---------------------------
 * @author RockYang<yangjian102621@gmail.com>
 */
class Manager
{
}
