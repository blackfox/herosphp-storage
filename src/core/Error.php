<?php

// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// * Copyright 2014 The Herosphp Authors. All rights reserved.
// * Use of this source code is governed by a MIT-style license
// * that can be found in the LICENSE file.
// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

declare(strict_types=1);

namespace herosphp\plugin\storage\core;

/**
 * 错误封装类
 * ---------------------------------------------------
 * @author RockYang<yangjian102621@gmail.com>
 */

final class Error
{
    protected int $code;

    protected string $message;

    public function __construct(int $code, string $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    public static function get(UploadError $error): self
    {
        return new self($error->value, $error->getMessage());
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
