<?php

declare(strict_types=1);

use herosphp\plugin\storage\handler\LocalFileHandler;
use herosphp\plugin\storage\handler\MinioFileSaveHandler;
use herosphp\plugin\storage\handler\QiniuFileSaveHandler;

return [
    // Allowed file extesion
    'allow_ext' => 'jpg|jpeg|png|gif|txt|pdf|rar|zip|swf|bmp|c|java|mp3',
    // Rejected file extension
    'reject_ext' => 'exe|sh|bat',
    // Allowed max file size, default value is  5MiB,
    // if no limits, set it to 0, default: 5MiB
    'max_size' => 5242880,

    'default_handler' => 'local',
    'handlers' => [
        'local' => [
            'class' => LocalFileHandler::class,
            'config' => [
                'root' => PUBLIC_PATH . 'upload',
                'url' => 'http://127.0.0.1:2345/upload/'
            ]
        ],

        'qiniu' => [
            'class' => QiniuFileSaveHandler::class,
            'config' => [
                'zone' => 'upload.qiniu.com',
                'access_key' => 'QINIU_ACCESS_KEY',
                'secret_key' => 'QINIU_SECRET_KEY',
                'bucket' => 'QINIU_BUCKET',
                'domain' => 'QINBIU_DOMAIN',
            ]
        ],

        'minio' => [
            'class' => MinioFileSaveHandler::class,
            'config' => [
                'credentials' => [
                    'key' => 'key',
                    'secret' => 'secret',
                ],
                'root' => date('Y/m/d'),
                'region' => '',
                'version' => 'latest',
                'bucket_endpoint' => false,
                'use_path_style_endpoint' => true,
                'endpoint' => 'http://172.28.1.51:9000',
                'bucket_name' => 'aas',
                'domain' => 'http://172.28.1.51:9000',
                'create_bucket' => true, //init upload,only run once.
                'is_set_policy' => true, //only true,set policies,run onec.
                'policies' => [  //options
                    'read+write' => ['*'], //@note must care permission
                ],
            ],
        ]
    ]

];
