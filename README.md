# herosphp/upload

## install
```shell
composer require herosphp/storage
```

## Usage

### qiniu
```php
composer require "qiniu/php-sdk:7.7.0"
```

### minio
```php
composer require "league/flysystem-aws-s3-v3:^3.0"
```
